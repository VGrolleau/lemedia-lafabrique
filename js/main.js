let navbarDropdown = document.getElementById('dropdown-nav');

function toggleDropdown() {
    if (!navbarDropdown.classList.contains('show')) {
        navbarDropdown.classList.add("show");
    } else {
        navbarDropdown.classList.remove("show");
    }
}
